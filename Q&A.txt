Q: Expliquer ce que sont  H, S et tail-rand

A: As I said several times yesterday, you can ignore these parameters for now.
The only parameter you need to consider is "c" the size of the view.

Simply refer to slide 36 of DS_2020-21_Gossip, and make the following assumptions:

function merge(): performs a set union operation between the arguments

function selectView(): extracts a subset of size "c" of the argument.
Here is where parameters H and S would play a role, but for this lab you can just ignore them. Simply do the following.
- For each group of duplicate entry(entries are duplicate if they have the same peer identifier) keep the entry with the newest timestamp.
- Select a random subset of "c" entries from the set, and use it as the new view.


Q: selectPeer() ça consiste à sélectionner aléatoirement le node actif et le node passif ou que le passif

A: Not sure I understand this question. In a real system, each node/peer/process operates independently of the others. Each of them uses selectPeer() to select a peer to communicate with in the  active cycle(see slide 36). The peer that is selected receives a message, and executes the passive cycle, sending a message back to the original peer.

In a cycle-based simulation like the one you are implementing, the active and passive cycles are collapsed togehter in a simulation cycle. But the behavior is the same.


Q: Pour la 3b, comment peut on à la fois sélectionner aléatoirement et les plus proches voisins

A: The question was indeed unclear: sorry. I have rephrased it as follows.
3b - Combine the code with each of the two protocols
# implemented in tasks 1 and 2, independently in two different experiments.
# Experiment with different network
# sizes and different view sizes.
# Does the shape of the degree distribution vary? Why?


Q:
I would personaly need another explanation of the 3
I still have difficulties to understand it.

A:
    # As an example consider the following network consisting of 5 nodes
I think you mean 3a. I have updated the lab question with an example as follows:
    # 1 has links to  2 4
    # 2 has links to 3 5
    # 3 has links to 1 2
    # 4 has links to 2 3
    # 5 has links to 1 4
    #
    # This leads to the following list of incoming links
    # 1 has links from 3 5
    # 2 has links from 1 3 4
    # 3 has links from 2 4
    # 4 has links from 1 5
    # 5 has links from 2
    #
    # So we have the following degree distribuition
    # 0 0
    # 1 1
    # 2 3
    # 3 1

We finish class at 4pm tomorrow we'll connect to teams asap
