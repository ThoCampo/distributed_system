from RPS_Exo1_2 import Network, Gossip
import matplotlib.pyplot as plt

# 3a- write code that outputs a distribution of in-degrees. In other words your
# code should output a list of lines with two values on each line.
# - The first value represents a given number of incoming neighbors
# - The second value represents the number of nodes that have that
# number of incoming neighbors.


def printDistrib(distribution):
    print("Distribution of in-degrees (number of incoming neighbours)")
    print("In-degree\t#Nodes")
    for elt in distribution:
        print(elt[0], "\t\t", elt[1])


def inDegrees(network):

    # Input : network
    nodes = network.getNodes()

    links_from = {}
    for node in nodes:
        links_from[node['ip']] = 0
    # Calculate number of links from for each node
    for node in nodes:
        for id in node['view']:
            links_from[id[0]] += 1

    distribution = {}
    # Calculate distribution : dictionary {in-degree: number of nodes}
    for id in links_from:
        if links_from[id] not in distribution:
            distribution[links_from[id]] = 1
        else:
            distribution[links_from[id]] += 1

    # Turn the dictionary in an ordered list of tuple [(,),(,),...]
    distribution_keys = []
    for key in distribution:
        if key not in distribution_keys:
            distribution_keys.append(key)
    distribution_list = []
    for i in range(max(distribution_keys)+1):
        if i in distribution:
            distribution_list.append((i, distribution[i]))
        else:
            distribution_list.append((i, 0))

    # Printing
    # print(nodes)
    # print(links_from)
    printDistrib(distribution_list)

    # Output : In-degrees distribution : list of tuple [(,),(,),...]
    return distribution_list


# Testing inDegrees() :
nw = Network(10)
distrib = inDegrees(nw)

# Computing examples to compare outputs:

# example 1 :
# 1 has links to  2 4
# 2 has links to 3 5
# 3 has links to 1 2
# 4 has links to 2 3
# 5 has links to 1 4
nw.setNodes([
    {'ip': 1, 'view': [(2, 0), (4, 0)]},
    {'ip': 2, 'view': [(3, 0), (5, 0)]},
    {'ip': 3, 'view': [(1, 0), (2, 0)]},
    {'ip': 4, 'view': [(2, 0), (3, 0)]},
    {'ip': 5, 'view': [(1, 0), (4, 0)]}
])
# This leads to the following list of incoming links
# 1 has links from 3 5
# 2 has links from 1 3 4
# 3 has links from 2 4
# 4 has links from 1 5
# 5 has links from 2
#
# So we have the following degree distribuition
# 0 0
# 1 1
# 2 3
# 3 1
inDegrees(nw)

# example 2 :
# 1 has links to  2 4 5
# 2 has links to 3 5
# 3 has links to 1 2
# 4 has links to 2 3
# 5 has links to 1 4
nw.setNodes([
    {'ip': 1, 'view': [(2, 0), (4, 0), (5, 0)]},
    {'ip': 2, 'view': [(3, 0), (5, 0)]},
    {'ip': 3, 'view': [(1, 0), (2, 0)]},
    {'ip': 4, 'view': [(2, 0), (3, 0)]},
    {'ip': 5, 'view': [(1, 0), (4, 0)]}
])
# This leads to the following list of incoming links
# 1 has links from 3 4
# 2 has links from 1 3 4
# 3 has links from 2 4
# 4 has links from 1 5
# 5 has links from 1 2
#
# So we have the following degree distribuition
# 0 0
# 1 0
# 2 4
# 3 1
inDegrees(nw)

# 3b - Combine the code with each of the two protocols
# implemented in tasks 1 and 2, independently in two different experiments.
# Experiment with different network
# sizes and different view sizes.
# Does the shape of the degree distribution vary? Why?


def plotDistrib(distribution, show=True):

    x = [i[0] for i in distribution]
    y = [i[1] for i in distribution]
    if show:
        plt.figure()
        plt.plot(x, y)
        plt.show()
    return (x, y)


# Testing plotDistrib() :
numNodes = 1000
c = 4
nw = Network(numNodes, c)
distrib = inDegrees(nw)
plotDistrib(distrib)


def experimentDistrib(numNodesList, cList, numCycles, strategy):
    fig, axs = plt.subplots(len(numNodesList), len(cList))
    suptitle = 'Strategy: ' + str(strategy)
    fig.suptitle(suptitle)
    for i in range(len(numNodesList)):
        for j in range(len(cList)):
            nw = Network(numNodesList[i], cList[j])
            gossip = Gossip(nw)
            gossip.reInit()
            gossip.pss.setStrategy(strategy)
            gossip.cycle(numCycles)
            distrib = inDegrees(nw)
            coord = plotDistrib(distrib, False)
            axs[i, j].grid()
            axs[i, j].set_xlabel('in-degrees')
            axs[i, j].set_ylabel('nodes')
            axs[i, j].plot(coord[0], coord[1])
            title = 'Network size: ' + \
                str(numNodesList[i]) + ', View size: ' + str(cList[j])
            axs[i, j].set_title(title)
    plt.show()


# Experimenting different network sizes and different view sizes
experimentDistrib([1000, 10000, 25000], [5, 10, 20], 100, 'rand')
experimentDistrib([1000, 10000, 25000], [5, 10, 20], 100, 'head')
experimentDistrib([1000, 10000, 25000], [5, 10, 20], 100, 'tail')
experimentDistrib([1000, 10000, 25000], [5, 10, 20], 100, 'knn')

# For each strategy and network size, the degree distributions approximate a Gaussian distribution.
# But the point is:
# The smaller the size of the view, the more the distribution will be spread to the right.
# Thus, for a large value of the view size, the distribution will be similar to a perfect Gaussian.

# So, the shape varies according to the size of the view.
