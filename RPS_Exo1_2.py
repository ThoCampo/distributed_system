# -*- coding: utf-8 -*-

from enum import Enum
from random import *


class Network():

    def __init__(self, numNodes, c=3):

        self.numNodes = numNodes
        self.c = c
        self.nodes = self.nodesInit()

    def nodesInit(self):

        nodes = []

        for i in range(0, self.numNodes):
            node = {}
            node['ip'] = i

            neighbours_possible = list(range(0, self.numNodes))
            neighbours_possible.remove(i)
            s = sample(neighbours_possible, self.c)
            node['view'] = list(map(lambda x: (x, 0), list(s)))

            nodes.append(node)

        return nodes

    def setNodes(self, nodes):
        self.nodes = nodes

    def getNodes(self):
        return(self.nodes)

    def headNodes(self, n=2):
        # Print the first n nodes
        print()
        for i in range(n):
            print('Node ', i, ' : ', self.nodes[i])


class PeerSamplingService():

    def __init__(self, network, strategy='rand', pull=False):
        self.network = network
        self.strategy = strategy  # could be 'rand','head' or 'tail'

    def getPeer(self):
        i = randrange(0, self.network.numNodes)
        p = self.network.getNodes()[i]
        return p

    def setStrategy(self, strategy):
        self.strategy = strategy

    # Active Cycle

    def activeCycle(self, node, timestamp, printed=False, pull=False):

        # Peer selection
        p = self.selectPeer(node)

        # Data Exchange
        myDescriptor = (node['ip'], timestamp)
        buffer = node['view'] + [myDescriptor]

        if(printed):
            print("\nACTIVE CYCLE")
            print("activeNode : ", node)
            print("buffer sent: ", buffer)

        # send buffer to p
        self.passiveCycle(p['ip'], buffer, node['ip'], timestamp,
                          pull=pull, printed=printed)

    def selectPeer(self, node):
        v = node['view']
        i = randrange(len(v))
        p = self.network.getNodes()[v[i][0]]
        return p

    # Passive Cycle

    def selectView(self, buffer, passiveNodeIP):

        b = []
        for elt in buffer:
            b.append(elt)

        # View selection

        # deletion of its own IP
        toRemove = []
        for e in b:
            if e[0] == passiveNodeIP:
                toRemove.append(e)
        for x in toRemove:
            b.remove(x)

        # selection depending on what strategy is used
        if(self.strategy == 'rand'):
            view = sample(b, self.network.c)

        elif(self.strategy == 'head'):
            view = []

            # taking the c 'youngest' nodes
            for k in range(self.network.c):
                maxi = 0
                indToSelect = 0
                for i in range(len(b)):
                    if b[i][1] > maxi:
                        maxi = b[i][1]
                        indToSelect = i

                view.append(b.pop(indToSelect))

        elif(self.strategy == 'tail'):
            view = []

            # taking the c 'oldest' nodes
            for k in range(self.network.c):
                mini = b[0][1]
                indToSelect = 0
                for i in range(len(b)):
                    if b[i][1] < mini:
                        mini = b[i][1]
                        indToSelect = i

                view.append(b.pop(indToSelect))

        elif(self.strategy == 'knn'):
            view = []

            # taking the c 'closest' nodes
            diff = []
            for k in range(len(b)):
                diff.append((abs(b[k][0]-passiveNodeIP), k))
            diff = sorted(diff, key=lambda x: x[0])
            diff = diff[:self.network.c]
            for ind in diff:
                view.append(b[ind[1]])

        return view

    def merge(self, view1, view2):

        v = view1 + view2

        doublons = []
        for i in range(len(v)):
            for j in range(i+1, len(v)):
                if v[i] == v[j]:
                    doublons.append(v[i])
                elif v[i][0] == v[j][0]:
                    if v[i][1] > v[j][1]:
                        doublons.append(v[j])
                    else:
                        doublons.append(v[i])
        for x in doublons:
            v.remove(x)

        return v

    def passiveCycle(self, passiveNodeIP, buffer, activeNodeIP, timestamp, pull, response=False, printed=False):
        nodes = self.network.getNodes()
        passiveNode = nodes[passiveNodeIP]

        if pull and not response:
            myDescriptor = (passiveNode['ip'], timestamp)
            buf = passiveNode['view'] + [myDescriptor]

            if(printed):
                print("\nPULL RESPONSE :")
                print("ACTIVE CYCLE")
                print("Node : ", passiveNode)
                print("buffer sent : ", buf)

            # send buffer to p
            self.passiveCycle(activeNodeIP, buf,
                              passiveNodeIP, timestamp, pull, response=True, printed=printed)

        # Data processing
        buffer = self.merge(buffer, passiveNode['view'])
        view = self.selectView(buffer, passiveNodeIP)

        if(printed):
            print("\nPASSIVE CYCLE")
            print("passiveNode : ", passiveNode)
            print("buffer : ", buffer)
            print("newView", view)

        nodes[passiveNodeIP]['view'] = view
        self.network.setNodes(nodes)


class Gossip():

    def __init__(self, network, numExchanges=10, pull=False):
        self.network = network
        self.pss = PeerSamplingService(network)
        self.numExchanges = numExchanges
        self.pull = pull  # False (-> Push only) or True (-> Push/Pull)
        self.timestamp = 0

    def reInit(self):
        self.timestamp = 0
        self.network.setNodes(self.network.nodesInit())

    def setNumExchanges(self, newNumExchanges):
        self.numExchanges = newNumExchanges

    def exchange(self, printed=False):
        p = self.pss.getPeer()
        self.pss.activeCycle(p, self.timestamp, printed, pull=self.pull)

    def cycle(self, numExchanges, printed=False):
        self.setNumExchanges(numExchanges)
        print("\nCYCLE of {} exchanges\npull = {}\nPear Sampling Service\nStrategy : {}".format(
            self.numExchanges, self.pull, self.pss.strategy))
        for i in range(numExchanges):
            self.exchange(printed)
            self.timestamp += 1


if __name__ == "__main__":

    # Preparing network and gossip
    numNodes = 10
    nw = Network(numNodes)
    gossip = Gossip(nw)

    # Example of an exchange
    gossip.exchange(printed=True)

    # Comparing views of 4 first nodes after 20 exchanges
    nw.headNodes(4)
    gossip.cycle(20)
    nw.headNodes(4)

    # Testing push/pull exchange:
    gossip.pull = True
    gossip.exchange(printed=True)
    gossip.pull = False

    # Testing Head strategy:
    gossip.reInit()
    gossip.pss.setStrategy('head')
    nw.headNodes(4)
    gossip.cycle(20)
    nw.headNodes(4)

    # Testing Tail strategy:
    gossip.reInit()
    gossip.pss.setStrategy('tail')
    nw.headNodes(4)
    gossip.cycle(20)
    nw.headNodes(4)

    # Testing KNN strategy:
    gossip.reInit()
    gossip.pss.setStrategy('knn')
    nw.headNodes(4)
    gossip.cycle(20)
    nw.headNodes(4)
