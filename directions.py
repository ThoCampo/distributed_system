# Create groups of 3 people to do the following,
# so that you  parallelize your tasks.
#
# 1- taking inspiration from  the simulator you implemented in the first lab,
# write a simulator for a peer-sampling protocol based on what you
# we described in the course. For simplicity you can select a fixed
# configuration with a specific parameter values for H, S, tail/rand, etc.
#
# 2- taking inspiration from  the simulator you implemented in the first lab,
# write a simulator for a KNN protocol. Instead of selecting random neighbors
# at each step, each node should select the nodes whose identifiers are
# numerically closest to its own identifier.
#
# 3a- write code that outputs a distribution of in-degrees. In other words your
# code should output a list of lines with two values on each line.
# - The first value represents a given number of incoming neighbors
# - The second value represents the number of nodes that have that
# number of incoming neighbors.
# As an example consider the following network consisting of 5 nodes
# 1 has links to  2 4
# 2 has links to 3 5
# 3 has links to 1 2
# 4 has links to 2 3
# 5 has links to 1 4
#
# This leads to the following list of incoming links
# 1 has links from 3 5
# 2 has links from 1 3 4
# 3 has links from 2 4
# 4 has links from 1 5
# 5 has links from 2
#
# So we have the following degree distribuition
# 0 0
# 1 1
# 2 3
# 3 1
#
#
#
#
# Another example:
# 1 has links to  2 4 5
# 2 has links to 3 5
# 3 has links to 1 2
# 4 has links to 2 3
# 5 has links to 1 4
#
# This leads to the following list of incoming links
# 1 has links from 3 4
# 2 has links from 1 3 4
# 3 has links from 2 4
# 4 has links from 1 5
# 5 has links from 1 2
#
# So we have the following degree distribuition
# 0 0
# 1 0
# 2 4
# 3 1
#
#
# 3b - Combine the code with each of the two protocols
# implemented in tasks 1 and 2, independently in two different experiments.
# Experiment with different network
# sizes and different view sizes.
# Does the shape of the degree distribution vary? Why?
#
# 4a - Combine the simulators you wrote in the first lab for
# push, pull, and push-pull dissemination with each of the two overlay
# protocols you implemented in task 1 and task 2.
# In particular, instead of selecting a random node from the entire network,
# a gossiping node should select a random node from its view
# (either the RPS view, or the KNN view).
#
# 4b - Study what happens when you run the three protocols with the two
# overlays. Use the metrics you implemented in the first lab and possibly
# the additoinal metrics we described in the course. Discuss your results.

#
# Intermediary Deadline: submit whatever you have done by Sunday November 1
# This won't be graded, but it will allow me to see what you have done and provide feedback.
# I can plan another Q/A during the week of Nov 2.
#
# Final Deadline: Sunday November 15.
#
