from enum import Enum
from random import *
from RPS_Exo1_2 import Network, Gossip, PeerSamplingService

# 4a - Combine the simulators you wrote in the first lab for
# push, pull, and push-pull dissemination with each of the two overlay
# protocols you implemented in task 1 and task 2.
# In particular, instead of selecting a random node from the entire network,
# a gossiping node should select a random node from its view
# (either the RPS view, or the KNN view).

# We reuse classes defined in exo 1 and 2 and we redefined functions which need to change.

class State(Enum):

    Susceptible=0
    Infected=1
    Recovered=2

class NetworkState(Network):

    def __init__(self, numNodes, c=3):

        Network.__init__(self, numNodes, c=3)
        self.nodes = self.stateInit()

    def stateInit(self):
        # Add a key 'state' for each node
        nodes = Network.nodesInit(self)
        for i in range(0, self.numNodes):
            nodes[i]['state'] = State.Susceptible
        infected = randrange(self.numNodes)
        nodes[infected]['state'] = State.Infected

        return nodes

# Testing NetworkState
numNodes = 10
nw = NetworkState(numNodes)
nw.headNodes(4)

class PeerSamplingServiceState(PeerSamplingService):

    def __init__(self, network, fanout, strategy='rand', pull=False):
        PeerSamplingService.__init__(self, network, strategy='rand', pull=False)
        self.fanout = fanout

    def selectPeer(self, node):
        # Select fanout peers
        v = node['view']
        if (len(v) <= self.fanout):
            toInfect = v
        else:
            toInfect = sample(v, self.fanout)
        p = []
        for i in range(len(toInfect)):
            p.append(self.network.getNodes()[toInfect[i][0]])
        return p

    def activeCycle(self, node, timestamp, printed=False, pull=False, push=True):

        # Peer selection
        p = self.selectPeer(node)

        # Data Exchange
        myDescriptor = (node['ip'], timestamp)
        buffer = node['view'] + [myDescriptor]

        if(printed):
            print("\nACTIVE CYCLE")
            print("activeNode : ", node)
            print("buffer sent: ", buffer)

        # send buffer to p
        recovered = False
        nodes = self.network.getNodes()
        for peer in p :
            if push:
                infect = self.passiveCycle(peer['ip'], buffer, node['ip'], timestamp,
                          pull=pull, printed=printed)
            # To do pull only, we do only the passive cycle by inverting passive and active node
            else:
                infect = self.passiveCycle(node['ip'], buffer, peer['ip'], timestamp,
                                           pull=pull, printed=printed)
            recovered = recovered or infect
        if recovered:
            if push:
                nodes[node['ip']]['state'] = State.Recovered
            else:
                nodes[peer['ip']]['state'] = State.Recovered
        self.network.setNodes(nodes)


    def passiveCycle(self, passiveNodeIP, buffer, activeNodeIP, timestamp, pull, response=False, printed=False):
        nodes = self.network.getNodes()
        passiveNode = nodes[passiveNodeIP]

        if pull and not response:
            myDescriptor = (passiveNode['ip'], timestamp)
            buf = passiveNode['view'] + [myDescriptor]

            if(printed):
                print("\nPULL RESPONSE :")
                print("ACTIVE CYCLE")
                print("Node : ", passiveNode)
                print("buffer sent : ", buf)

            # send buffer to p
            self.passiveCycle(activeNodeIP, buf,
                              passiveNodeIP, timestamp, pull, response=True, printed=printed)

        # Data processing
        buffer = self.merge(buffer, passiveNode['view'])
        view = self.selectView(buffer, passiveNodeIP)

        if(printed):
            print("\nPASSIVE CYCLE")
            print("passiveNode : ", passiveNode)
            print("buffer : ", buffer)
            print("newView", view)

        nodes[passiveNodeIP]['view'] = view
        infect = False
        if nodes[activeNodeIP]['state'] == State.Infected and nodes[passiveNodeIP]['state'] == State.Susceptible:
            nodes[passiveNodeIP]['state'] = State.Infected
            infect = True

        self.network.setNodes(nodes)
        return infect

class GossipState(Gossip):

    def __init__(self, network, fanout=3, numExchanges=10, pull=False, push=True):
        Gossip.__init__(self, network, numExchanges=10, pull=False)
        self.pss = PeerSamplingServiceState(network, fanout)
        self.push = push # False with pull=False --> pull only, True with pull=False --> push only, True with pull=True --> push/pull

    def reInit(self):
        self.timestamp = 0
        self.network.setNodes(self.network.stateInit())

    def countState(self):
        nodes = self.network.getNodes()
        state = []
        for node in nodes:
            state.append(node['state'])
        s = state.count(State.Susceptible)
        i = state.count(State.Infected)
        r = state.count(State.Recovered)
        stateCount = {'susceptible': s, 'infected': i, 'recovered': r}
        return stateCount

    def exchange(self, printed=False):
        nodes = self.network.getNodes()
        for p in nodes:
            self.pss.activeCycle(p, self.timestamp, printed, pull=self.pull)

    def cycle(self, numExchanges, printed=False):
        self.setNumExchanges(numExchanges)
        print("\nCYCLE of {} exchanges\npush = {}\npull = {}\nPear Sampling Service\nStrategy : {}".format(
            self.numExchanges, self.push, self.pull, self.pss.strategy))
        for i in range(numExchanges):
            self.exchange(printed)
            self.timestamp += 1
            stateCount = self.countState()
            prop = stateCount['infected']/self.network.numNodes*100
            propTot = (stateCount['infected']+stateCount['recovered'])/self.network.numNodes*100
            print("cycle {}: S: {}, I: {}, R: {} --> Proportion of infected nodes: {}%, proportion of infected and recovered nodes: {}%.".format(i, stateCount['susceptible'],
                                                          stateCount['infected'], stateCount['recovered'], prop, propTot))



# 4b - Study what happens when you run the three protocols with the two
# overlays. Use the metrics you implemented in the first lab and possibly
# the additoinal metrics we described in the course. Discuss your results.

def strategiesTest(numNodes, numCycles, overlayList):
    nw = NetworkState(numNodes)
    gossip = GossipState(nw)
    for overlay in overlayList:
        gossip.pss.setStrategy(overlay)
        # Push only
        gossip.push = True
        gossip.pull = False
        gossip.cycle(numCycles)

        gossip.reInit()
        # Pull only
        gossip.push = False
        gossip.pull = False
        gossip.cycle(numCycles)

        gossip.reInit()
        # Push/pull
        gossip.push = True
        gossip.pull = True
        gossip.cycle(numCycles)
        gossip.reInit()

strategiesTest(1000, 20, ['rand', 'head', 'tail', 'knn'])

# Rand + push only: 94%
# Rand + pull only: 82%
# Rand + push/pull: 100%

# Head + push only: 2%
# Head + pull only: 6%
# Head + push/pull: 90%

# Tail + push only: 37%
# Tail + pull only: 41%
# Tail + push/pull: 99%

# KNN + push only: 1%
# KNN + pull only: 2%
# KNN + push/pull: 99%

# Random strategy works well with all dissemination strategies (push, pull, push/pull).
# Head strategy is the least efficient.
# Tail strategy works well with push/pull dissemination strategy.
# KNN strategy is ineffective with push and pull dissemination strategies because each node communicates only with neighbours which are close to it.

# Push/push dissemination is the best way to spread information.




